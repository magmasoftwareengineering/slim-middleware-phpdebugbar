<?php

use Composer\Autoload\ClassLoader;
use DebugBar\Bridge\DoctrineCollector;
use DebugBar\Bridge\MonologCollector;
use DebugBar\Bridge\NamespacedTwigProfileCollector;
use DebugBar\Bridge\Symfony\SymfonyMailCollector;
use DebugBar\DataCollector\ConfigCollector;
use DebugBar\DataCollector\ExceptionsCollector;
use DebugBar\DataCollector\MemoryCollector;
use DebugBar\DataCollector\MessagesCollector;
use DebugBar\DataCollector\PDO\PDOCollector;
use DebugBar\DataCollector\PDO\TraceablePDO;
use DebugBar\DataCollector\PhpInfoCollector;
use DebugBar\DataCollector\RequestDataCollector;
use DebugBar\DataCollector\TimeDataCollector;
use DebugBar\DebugBar;
use Doctrine\DBAL\Logging\DebugStack;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManagerInterface;
use MagmaSoftwareEngineering\PhpDebugBar\Controller\PhpDebugBarController;
use MagmaSoftwareEngineering\PhpDebugBar\Resources\Twig\Extension\DebugBarRender;
use MagmaSoftwareEngineering\PhpDebugBar\Storage\RedisHashStorage;
use Monolog\Logger;
use PhpMiddleware\PhpDebugBar\ConfigProvider;
use Predis\ClientInterface;
use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Log\LoggerInterface;
use Slim\HttpCache\CacheProvider;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Interfaces\RouteGroupInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Views\Twig;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Mailer\Event\SentMessageEvent;
use Twig\Extension\ProfilerExtension;
use Twig\Profiler\Profile;

/** @var Container $container */
if (class_exists('PhpMiddleware\PhpDebugBar\ConfigProvider')) {
    $containerArray = [
        'debugBar.enable' => DI\env('DEBUGBAR_ENABLE', false),
        'debugBar.cache' => DI\env('DEBUGBAR_CACHE', false),
        'debugBar.doctrine' => DI\env('DEBUGBAR_DOCTRINE', false),
        'debugBar.monolog' => DI\env('DEBUGBAR_MONOLOG', false),
        'debugBar.pdo' => DI\env('DEBUGBAR_PDO', false),
        'debugBar.slim.routes' => DI\env('DEBUGBAR_SLIM_ROUTES', false),
        'debugBar.slim.settings' => DI\env('DEBUGBAR_SLIM_SETTINGS', false),
        'debugBar.symfony.mailer' => DI\env('DEBUGBAR_SYMFONY_MAILER', false),
        'debugBar.twig' => DI\env('DEBUGBAR_TWIG', false),
    ];

    foreach (ConfigProvider::getConfig()['dependencies']['factories'] as $key => $factory) {
        $containerArray[$key] = DI\factory(function (Container $container) use ($key, $factory) {
            return (new $factory())($container);
        });
    }

    $containerArray = array_merge($containerArray, [
        ConfigProvider::class => DI\decorate(function ($previous, $container): array {
            return array_merge(
                $previous(),
                [
                    'phpmiddleware' => [
                        'phpdebugbar' => [
                            'javascript_renderer' => [
                                'base_url' => '/phpdebugbar',
                            ],
                            'collectors' => [
                                // Service names of collectors
                                MessagesCollector::class,
                                TimeDataCollector::class,
                                ExceptionsCollector::class,
                                MemoryCollector::class,
                                PhpInfoCollector::class,
                                ConfigCollector::class,
                                // Doctrine / PDO mutually exclusive for now
                                filter_var($container->get('debugBar.doctrine'), FILTER_VALIDATE_BOOLEAN) &&
                                !filter_var($container->get('debugBar.pdo'), FILTER_VALIDATE_BOOLEAN) ?
                                    DoctrineCollector::class :
                                    null,
                                filter_var($container->get('debugBar.pdo'), FILTER_VALIDATE_BOOLEAN) &&
                                !filter_var($container->get('debugBar.doctrine', FILTER_VALIDATE_BOOLEAN)) ?
                                    PDOCollector::class :
                                    null,
                                filter_var($container->get('debugBar.monolog'), FILTER_VALIDATE_BOOLEAN) ?
                                    MonologCollector::class :
                                    null,
                                RequestDataCollector::class,
                                ConfigCollector::class . ':SuperGlobals',
                                filter_var($container->get('debugBar.symfony.mailer'), FILTER_VALIDATE_BOOLEAN) ?
                                    SymfonyMailCollector::class :
                                    null,
                            ],
                            'storage' => filter_var($container->get('debugBar.cache'), FILTER_VALIDATE_BOOLEAN) ?
                                // Service name of storage
                                RedisHashStorage::class :
                                null,
                        ],
                    ],
                ]
            );
        }),
        Configuration::class => DI\decorate(function (Configuration $previous, $container): Configuration {
            $previous->setSQLLogger(new DebugStack());
            return $previous;
        }),
        DoctrineCollector::class => DI\create()
            ->constructor(DI\get(EntityManagerInterface::class)),
        RedisHashStorage::class => DI\autowire()
            ->constructorParameter('redis', DI\get(ClientInterface::class)),
        PDOCollector::class => DI\factory(function (Container $container) {
            $traceablePDO = new TraceablePDO($container->get(PDO::class));
            return new PDOCollector($traceablePDO, $container->get(TimeDataCollector::class));
        }),
        ConfigCollector::class . ':SuperGlobals' => DI\factory(function (Container $container): ConfigCollector {
            $sources = [
                '$_ENV' => $_ENV,
                '$_SERVER' => $_SERVER,
                '$_FILES' => $_FILES,
            ];
            return new ConfigCollector(
                $sources,
                '+SuperGlobals'
            );
        }),
        PhpDebugBarController::class => DI\factory(function (Container $container): PhpDebugBarController {
            $debugBar = new PhpDebugBarController();

            if ($container->has(LoggerInterface::class)) {
                $logger = $container->get(LoggerInterface::class);
                if ($logger instanceof Logger) {
                    $logger = $logger->withName($logger->getName() . '.DebugBar');
                }
                $debugBar->setLogger($logger);
            }

            $debugBar->setModuleSetting($debugBar->getEnvEnableKey(), $container->get('debugBar.enable'));

            if ($container->has($debugBar->getDebugBarDIKey())) {
                $debugBar->setModuleSetting($debugBar->getDebugBarDIKey(), $container->get($debugBar->getDebugBarDIKey()));
            }

            if (
                $container->get('debugBar.cache') === $debugBar->getEnvCacheValue() &&
                !$container->has($debugBar->getCacheDIKey())
            ) {
                /** @var DI\Container $container */
                $container->set($debugBar->getCacheDIKey(), static function () {
                    return new CacheProvider();
                });
            }

            if ($container->has($debugBar->getCacheDIKey())) {
                $debugBar->setModuleSetting($debugBar->getCacheDIKey(), $container->get($debugBar->getCacheDIKey()));
            }

            return $debugBar;
        }),
        'PhpDebugBarSlimMiddleware' => DI\factory(function (
            Container $container,
            RouteParserInterface $routeParser,
            ?DebugBar $debugBar = null
        ): callable {
            return function (Request $request, RequestHandler $handler) use (
                $container,
                $routeParser,
                $debugBar
            ): Response {
                if ($debugBar !== null) {
                    // Set up PhpDebugBar/Slim
                    if (
                        !$debugBar->hasCollector('twig') &&
                        filter_var($container->get('debugBar.twig'), FILTER_VALIDATE_BOOLEAN)
                    ) {
                        $twigCollector = $container->get(NamespacedTwigProfileCollector::class);
                        if ($twigCollector !== null) {
                            $debugBar->addCollector($twigCollector);
                        }
                    }
                    if (
                        !$debugBar->hasCollector('SlimRoutes') &&
                        filter_var($container->get('debugBar.slim.routes'), FILTER_VALIDATE_BOOLEAN)
                    ) {
                        $debugBar->addCollector($container->get(ConfigCollector::class . ':SlimRoutes'));
                    }
                    if (
                        !$debugBar->hasCollector('SlimSettings') &&
                        filter_var($container->get('debugBar.slim.settings', FILTER_VALIDATE_BOOLEAN))
                    ) {
                        $debugBar->addCollector($container->get(ConfigCollector::class . ':SlimSettings'));
                    }
                    $debugBarRenderer = $debugBar->getJavascriptRenderer();
                    $debugBarRenderer->setOpenHandlerUrl($routeParser->urlFor('phpdebugbarOpen'));
                }

                $response = $handler->handle($request);

                if ($debugBar !== null && PHP_SAPI !== 'cli' && $request->hasHeader('accept')) {
                    $header = $request->getHeader('accept')[0];
                    if (
                        $request->getHeaderLine('X-Requested-With') === 'XMLHttpRequest' ||
                        str_contains($header, 'application/json')
                    ) {
                        $debugBar->sendDataInHeaders(true);
                    }
                }

                return $response;
            };
        }),
        DebugBar::class => DI\decorate(function (DebugBar $previous, Container $container): DebugBar {
            $config = $container->get(ConfigProvider::class);
            foreach ($config['phpmiddleware']['phpdebugbar']['collectors'] as $collector) {
                if ($collector !== null && $container->has($collector)) {
                    $collectorParts = explode(':', $collector, 1);
                    $collectorName = $collectorParts[0];
                    if (count($collectorParts) === 2) {
                        $collectorName = $collectorParts[1];
                    }
                    if (!$previous->hasCollector($collectorName)) {
                        $previous->addCollector($container->get($collector));
                    }
                }
            }

            $previous->setStorage(
                $config['phpmiddleware']['phpdebugbar']['storage'] !== null ?
                    $container->get($config['phpmiddleware']['phpdebugbar']['storage']) :
                    null
            );
            $previous->getJavascriptRenderer(
                $config['phpmiddleware']['phpdebugbar']['javascript_renderer']['base_url']
            );

            return $previous;
        }),
        // Only call DebugBar::addCollector for this in middleware, otherwise, PHP-DI circular dependency error occurs
        ConfigCollector::class . ':SlimSettings' => DI\factory(function (
            Container $container,
            ClassLoader $loader
        ): ConfigCollector {
            // Slim Settings
            $clonedSettings = [];
            $classMap = $loader->getClassMap();

            /** @var DI\Container $container */
            foreach ($container->getKnownEntryNames() as $key => $value) {
                if (
                    !array_key_exists($value, $classMap) &&
                    !class_exists($value) &&
                    !str_contains($value, ConfigCollector::class) &&
                    !is_callable($container->get($value))
                ) {
                    $clonedSettings[$value] = $container->get($value);
                }
            }
            ksort($clonedSettings);
            return new ConfigCollector($clonedSettings, 'SlimSettings');
        }),
        // Only call DebugBar::addCollector for this in middleware, otherwise, not all routes will be available
        ConfigCollector::class . ':SlimRoutes' => DI\factory(function (Container $container): ConfigCollector {
            /** @var RouteCollectorInterface $collector */
            $collector = $container->get(RouteCollectorInterface::class);
            $routes = [];
            foreach ($collector->getRoutes() as $route) {
                $name = $route->getName();
                $pattern = $route->getPattern();
                $routes[$name ?? $pattern] = [
                    'pattern' => $route->getPattern(),
                    'name' => $name,
                    'identifier' => $route->getIdentifier(),
                    'methods' => $route->getMethods(),
                    'groups' => array_map(
                        function(RouteGroupInterface $group) {
                            return $group->getPattern();
                        },
                        $route->getGroups()
                    ),
                    'arguments' => $route->getArguments(),
                ];
            }
            ksort($routes);
            return new ConfigCollector($routes, 'SlimRoutes');
        }),
        LoggerInterface::class => DI\decorate(function (
            LoggerInterface $previous,
            Container $container
        ): LoggerInterface {
            if ($previous instanceof Logger) {
                /** @var MonologCollector $monologCollector */
                $monologCollector = $container->get(MonologCollector::class);
                $monologCollector->addLogger($previous);
            }
            return $previous;
        }),
        SymfonyMailCollector::class => DI\autowire()
            ->method('showMessageBody'),
        EventDispatcherInterface::class => DI\decorate(function ($previous, Container $container) {
            $mailCollector = $container->get(SymfonyMailCollector::class);
            $previous->addListener(
                SentMessageEvent::class,
                function (SentMessageEvent $event) use ($mailCollector): void {
                    $mailCollector->addSymfonyMessage($event->getMessage());
                }
            );
            return $previous;
        }),
        NamespacedTwigProfileCollector::class => DI\factory(function (Container $container, Twig $twig) {
            $loader = $twig->getLoader();
            $env = $twig->getEnvironment();
            if (
                $env->hasExtension(ProfilerExtension::class) &&
                $container->has(Profile::class)
            ) {
                $profile = $container->get(Profile::class);
                return new NamespacedTwigProfileCollector($profile, $loader);
            }
            return null;
        }),
        Twig::class => DI\decorate(function (Twig $previous, Container $container): Twig {
            $env = $previous->getEnvironment();
            if (!$env->hasExtension(DebugBarRender::class)) {
                $previous->addExtension($container->get(DebugBarRender::class));
            }
            return $previous;
        }),
    ]);

    return $containerArray;
}

return [];
