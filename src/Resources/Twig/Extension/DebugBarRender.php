<?php

namespace MagmaSoftwareEngineering\PhpDebugBar\Resources\Twig\Extension;

use DebugBar\DebugBar;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class DebugBarRender extends AbstractExtension implements GlobalsInterface
{
    public function __construct(
        private readonly DebugBar $debugBar
    ) {
    }

    public function getGlobals(): array
    {
        $this->debugBar->collect();
        return [
            'debugBar' => [
                'header' => $this->debugBar->getJavascriptRenderer()->renderHead(),
                'footer' => $this->debugBar->getJavascriptRenderer()->render(),
            ]
        ];
    }
}
