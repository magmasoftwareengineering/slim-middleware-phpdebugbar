<?php

namespace MagmaSoftwareEngineering\PhpDebugBar\Storage;

use DebugBar\Storage\StorageInterface;
use Predis\Client;

class RedisHashStorage implements StorageInterface
{
    protected Client $redis;

    protected string $hash;

    protected int $timeout;

    public function __construct(Client $redis, string $hash = 'phpdebugbar:', int $timeout = 3600)
    {
        $this->redis = $redis;
        $this->hash = $hash;
        $this->timeout = $timeout;
    }

    /**
     * {@inheritdoc}
     */
    public function save($id, $data): void
    {
        $this->redis->setex($this->hash . $id, $this->timeout, serialize($data));
    }

    /**
     * {@inheritdoc}
     */
    public function get($id): mixed
    {
        return unserialize($this->redis->get($this->hash . $id));
    }

    /**
     * {@inheritdoc}
     */
    public function find(array $filters = [], $max = 20, $offset = 0): array
    {
        $results = [];
        $keys = $this->redis->keys($this->hash . '*');
        $prefix = $this->redis->getOptions()->prefix ?? '';
        foreach ($keys as $key) {
            if (!empty($prefix)) {
                $key = \preg_replace('`' . $prefix . '`', '', $key);
            }
            $data = $this->redis->get($key);
            if (!is_null($data)) {
                $data = unserialize($data);
            }
            if ($data) {
                $meta = $data['__meta'];
                if ($this->filter($meta, $filters)) {
                    $results[(string)$meta['utime']] = $meta;
                }
            }
        }
        \krsort($results, SORT_NUMERIC);
        return array_slice($results, $offset, $max);
    }

    /**
     * Filter the metadata for matches.
     */
    protected function filter($meta, $filters): bool
    {
        foreach ($filters as $key => $value) {
            if (!isset($meta[$key]) || fnmatch($value, $meta[$key]) === false) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function clear(): void
    {
        $keys = $this->redis->keys($this->hash . '*');
        $this->redis->del($keys);
    }
}
