<?php

namespace MagmaSoftwareEngineering\PhpDebugBar\Controller;

use DateTime;
use DebugBar\DebugBar;
use DebugBar\DebugBarException;
use DebugBar\JavascriptRenderer;
use DebugBar\OpenHandler;
use DebugBar\StandardDebugBar;
use MagmaSoftwareEngineering\Slim\Controller\AbstractController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use RuntimeException;
use Slim\HttpCache\CacheProvider;
use function base64_encode;

/**
 * Class PhpDebugBarController
 * @package MagmaSoftwareEngineering\PhpDebugBar\Controller
 */
class PhpDebugBarController extends AbstractController
{
    private string $envEnableKey = 'DEBUGBAR_ENABLE';
    private string $envEnableValue = 'true';
    private string $debugBarDIKey = DebugBar::class;

    private string $envCacheKey = 'DEBUGBAR_CACHE';
    private string $envCacheValue = 'true';
    private string $cacheDIKey = 'cache';

    public function getEnvEnableKey(): string
    {
        return $this->envEnableKey;
    }

    public function setEnvEnableKey(string $envEnableKey): self
    {
        $this->envEnableKey = $envEnableKey;
        return $this;
    }

    public function getEnvEnableValue(): string
    {
        return $this->envEnableValue;
    }

    public function setEnvEnableValue(string $envEnableValue): self
    {
        $this->envEnableValue = $envEnableValue;
        return $this;
    }

    public function getEnvCacheKey(): string
    {
        return $this->envCacheKey;
    }

    public function setEnvCacheKey(string $envCacheKey): self
    {
        $this->envCacheKey = $envCacheKey;
        return $this;
    }

    public function getEnvCacheValue(): string
    {
        return $this->envCacheValue;
    }

    public function setEnvCacheValue(string $envCacheValue): self
    {
        $this->envCacheValue = $envCacheValue;
        return $this;
    }

    public function getDebugBarDIKey(): string
    {
        return $this->debugBarDIKey;
    }

    public function setDebugBarDIKey(string $debugBarDIKey): self
    {
        $this->debugBarDIKey = $debugBarDIKey;
        return $this;
    }

    public function getCacheDIKey(): string
    {
        return $this->cacheDIKey;
    }

    public function setCacheDIKey(string $cacheDIKey): self
    {
        $this->cacheDIKey = $cacheDIKey;
        return $this;
    }

    /**
     * @throws RuntimeException
     */
    public function jsHeader(Request $request, Response $response): Response
    {
        $content = '';

        if (
            $this->hasModuleSetting($this->envEnableKey) &&
            $this->getModuleSetting($this->envEnableKey) === $this->envEnableValue &&
            $this->hasModuleSetting($this->debugBarDIKey)
        ) {
            /** @var StandardDebugBar $debugBar */
            $debugBar = $this->getModuleSetting($this->debugBarDIKey);

            $renderer = $debugBar->getJavascriptRenderer();

            if ($this->hasModuleSetting($this->envCacheKey)) {
                if ($this->getModuleSetting($this->envCacheKey) === $this->envCacheValue) {
                    if ($this->hasModuleSetting($this->cacheDIKey)) {
                        $cache = $this->getModuleSetting($this->cacheDIKey);
                        if ($cache instanceof CacheProvider) {
                            $response = $cache->withEtag(
                                $response,
                                'phpdebugbarJsHeader_' . base64_encode((new DateTime())->format('Y-m-d'))
                            );
                        }
                    }
                }
            }

            ob_start();
            $renderer->dumpJsAssets();
            $content = ob_get_clean();
        }

        $response->getBody()->write($content);

        return $response->withHeader('Content-Type', 'text/javascript');
    }

    /**
     * @throws RuntimeException
     */
    public function cssHeader(Request $request, Response $response): Response
    {
        $content = '';

        if (
            $this->hasModuleSetting($this->envEnableKey) &&
            $this->getModuleSetting($this->envEnableKey) === $this->envEnableValue &&
            $this->hasModuleSetting($this->debugBarDIKey)
        ) {
            /** @var StandardDebugBar $debugBar */
            $debugBar = $this->getModuleSetting($this->debugBarDIKey);

            $renderer = $debugBar->getJavascriptRenderer();

            if ($this->hasModuleSetting($this->envCacheKey)) {
                if ($this->getModuleSetting($this->envCacheKey) === $this->envCacheValue) {
                    if ($this->hasModuleSetting($this->cacheDIKey)) {
                        $cache = $this->getModuleSetting($this->cacheDIKey);
                        if ($cache instanceof CacheProvider) {
                            $response = $cache->withEtag(
                                $response,
                                'phpdebugbarCssHeader_' . base64_encode((new DateTime())->format('Y-m-d'))
                            );
                        }
                    }
                }
            }

            ob_start();
            $renderer->dumpCssAssets();
            $content = ob_get_clean();
        }

        $response->getBody()->write($content);

        return $response->withHeader('Content-Type', 'text/css');
    }

    /**
     * @throws RuntimeException
     */
    public function renderHead(Request $request, Response $response): Response
    {
        $content = '';

        if (
            $this->hasModuleSetting($this->envEnableKey) &&
            $this->getModuleSetting($this->envEnableKey) === $this->envEnableValue &&
            $this->hasModuleSetting($this->debugBarDIKey)
        ) {
            /** @var StandardDebugBar $debugBar */
            $debugBar = $this->getModuleSetting($this->debugBarDIKey);

            $renderer = $debugBar->getJavascriptRenderer();

            if ($this->hasModuleSetting($this->envCacheKey)) {
                if ($this->getModuleSetting($this->envCacheKey) === $this->envCacheValue) {
                    if ($this->hasModuleSetting($this->cacheDIKey)) {
                        $cache = $this->getModuleSetting($this->cacheDIKey);
                        if ($cache instanceof CacheProvider) {
                            $response = $cache->withEtag(
                                $response,
                                'phpdebugbarRenderHead_' . base64_encode((new DateTime())->format('Y-m-d'))
                            );
                        }
                    }
                }
            }

            $content = $renderer->renderHead();
            $content = preg_replace('`^<script type="text/javascript">(.*)`m', '\\1', $content);
            $content = preg_replace('`(.*)</script>$`m', '\\1', $content);
        }

        $response->getBody()->write($content);

        return $response->withHeader('Content-Type', 'text/javascript');
    }

    /**
     * @throws RuntimeException
     */
    public function render(Request $request, Response $response): Response
    {
        $content = 'var phpdebugbar;';// Needed for ng-phpdebugbar

        if (
            $this->hasModuleSetting($this->envEnableKey) &&
            $this->getModuleSetting($this->envEnableKey) === $this->envEnableValue &&
            $this->hasModuleSetting($this->debugBarDIKey)
        ) {
            /** @var StandardDebugBar $debugBar */
            $debugBar = $this->getModuleSetting($this->debugBarDIKey);

            $uri = $request->getUri();
            $port = $uri->getPort();
            $port = null === $port ? '' : ':' . $port;

            $renderer = $debugBar->getJavascriptRenderer();
            $url = $uri->getScheme() . '://' . $uri->getHost() . $port . $renderer->getBaseUrl() . '/open';
            $renderer->setOpenHandlerUrl($url);

            $content = $renderer->render();
            $content = preg_replace('`^<script type="text/javascript">(.*)`m', '\\1', $content);
            $content = preg_replace('`(.*)</script>$`m', '\\1', $content);
        }

        $response->getBody()->write($content);

        return $response->withHeader('Content-Type', 'text/javascript');
    }

    /**
     * @throws DebugBarException
     * @throws RuntimeException
     */
    public function open(Request $request, Response $response): Response
    {
        $content = '';

        if ($this->hasModuleSetting($this->debugBarDIKey)) {
            /** @var StandardDebugBar $debugBar */
            $debugBar = $this->getModuleSetting($this->debugBarDIKey);

            $openHandler = new OpenHandler($debugBar);
            $content = $openHandler->handle($request->getQueryParams(), false, false);
        }

        $response->getBody()->write($content);

        return $response;
    }
}
