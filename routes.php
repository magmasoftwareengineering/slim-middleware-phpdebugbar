<?php

declare(strict_types=1);

use MagmaSoftwareEngineering\PhpDebugBar\Controller\PhpDebugBarController;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface;

/** @var App $app */
$app->group('/phpdebugbar', function (RouteCollectorProxyInterface $group) {
    $group->get('/jsheader.js', PhpDebugBarController::class . ':jsHeader')
        ->setName('phpdebugbarJsHeader');

    $group->get('/vendor/font-awesome/css/header.css', PhpDebugBarController::class . ':cssHeader')
        ->setName('phpdebugbarCssHeader');

    $group->get('/render.js', PhpDebugBarController::class . ':render')
        ->setName('phpdebugbarRender');

    $group->get('/open', PhpDebugBarController::class . ':open')
        ->setName('phpdebugbarOpen');

    $group->get('/renderhead.js', PhpDebugBarController::class . ':renderHead')
        ->setName('phpdebugbarRenderHead');
});
