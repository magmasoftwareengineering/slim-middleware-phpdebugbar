# PHPDebugBar Slim3 Middleware

## Requirements

#### On the PHP side:

* Require the php-debug-bar package

```bash
composer require php-middleware/php-debug-bar
```

* Ensure `setenv('DEBUGBAR_ENABLE', 'true')` is set
* Ensure `setenv('DEBUGBAR_CACHE', 'true')` is set, if you want to enable caching / eTag output
* Ensure `redis` is available & running

* If you see a message similar to:

```
Font from origin 'http://project.local' has been blocked from loading by Cross-Origin Resource Sharing policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. Origin 'http://project.local:8556' is therefore not allowed access.
```

* Update your `.htaccess` file:

```bash
# Allow access from all domains for webfonts.
# Alternatively you could only whitelist your
# subdomains like "subdomain.example.com".
<IfModule mod_headers.c>
  <FilesMatch "\.(ttf|ttc|otf|eot|woff|woff2|font.css|css)$">
    Header set Access-Control-Allow-Origin "*"
  </FilesMatch>
</IfModule>
```

* Make sure that you have `redis` running

#### On the Angular side:
* Require the ng-phpdebugbar package

```bash
bower install --save ng-phpdebugbar
```

* Require the 'ng-phpdebugbar' module and you are all set

```javascript
angular.module('myApp', ['ng-phpdebugbar'])
```

* Add the calls to the `<head/>` in `index.html`

```html
<link rel="stylesheet" type="text/css" href="http://project.local/phpdebugbar/vendor/font-awesome/css/header.css">
<script src="http://project.local/phpdebugbar/jsheader.js"></script>
```

* Add the call to the renderer in the `<body/>` in `index.html`

```html
<script src="http://project.local/phpdebugbar/render.js"></script>
```

##### Alternative inclusion (useful if your API is not on the same port/URL as the angular side)

* Remove any angular auto-bootstrapping, we'll handle that ourselves

```html
<head ng-app="appModule">
...
</head>
```

becomes:

```html
<head>
...
</head>
```

* In the body include client/app/phpDebugbar.js which will load up the required css/Js dynamically:

```html
<body>
<script src="bower_components/ng-phpdebugbar/ng-phpdebugbar.js"></script>
<script src="clients/app/app.js"></script>
<script src="client/app/phpDebugbar.js"></script>
...
```

Also, make sure you return the angular module to the global window object:

```javascript
var angularAppModule = angular
    .module('appModule', [
...
 window.angularAppModule = angularAppModule;
})(window.angular); 
```

#### Seeing it all together

* Fetch debugbar from the DI Container. 

```php
/** @var \DebugBar\StandardDebugBar $debugbar */
$debugbar = $container->get('debugbar');
```

* Then use it to measure timestamps 

```php
$debugbar['time']->startMeasure(__METHOD__);
...
$debugbar['time']->stopMeasure(__METHOD__);
```

* Or set messages for collection

```php
$debugbar['messages']->info('Some string or value');
```

* You're ready to begin adding messages, time date and exception data. Enjoy!

![Screenshot with PHPDebugBar](ng-phpdebugbar.png?raw=true)
