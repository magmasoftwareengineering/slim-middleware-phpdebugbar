<?php

use PhpMiddleware\PhpDebugBar\PhpDebugBarMiddleware;
use Psr\Container\ContainerInterface as Container;
use Slim\App;

if (class_exists('PhpMiddleware\PhpDebugBar\PhpDebugBarMiddleware')) {
    /** @var App $app */
    /** @var Container $container */
    $app->add($container->get(PhpDebugBarMiddleware::class));

    $app->add($container->get('PhpDebugBarSlimMiddleware'));
}
