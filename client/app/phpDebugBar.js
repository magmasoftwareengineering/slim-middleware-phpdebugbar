function elementLoader(url, type = 'script', elName = 'head', rel = 'script', preloadAs = 'style', onLoad = null)
{
    //Preload: type = link, rel = 'preload', preloadAs = 'script/style'
    //Css: type = link, rel = 'stylesheet', href = url
    //Js: type = script, src = url

    var targetElement = document.getElementsByTagName(elName)[0];
    var createdElement = document.createElement(type);
    if (type === 'script') {
        createdElement.setAttribute('src', url);
    } else {
        createdElement.href = url;
        createdElement.rel = rel;
        if (rel === 'preload') {
            createdElement.as = preloadAs;
        }
    }
    if (onLoad !== null) {
        createdElement.onload = onLoad;
    }
    targetElement.appendChild(createdElement);
}

(function (window, angular, angularAppModule) {
    'use strict';

    if (window.__env.debugMode) {
        // Force the correct load order via onLoad callbacks
        elementLoader(window.__env.apiUrl + 'phpdebugbar/jsheader.js', 'script', 'body', 'script', '', function () {
            elementLoader(window.__env.apiUrl + 'phpdebugbar/render.js', 'script', 'body', 'script', '', function () {
                elementLoader(
                    'bower_components/ng-phpdebugbar/ng-phpdebugbar.js',
                    'script',
                    'body',
                    'script',
                    '',
                    function () {
                        angularAppModule.requires.push('ng-phpdebugbar');
                        angular.bootstrap(document, ['appModule']);
                    }
                );
            });
        });

        elementLoader(
            window.__env.apiUrl + 'phpdebugbar/vendor/font-awesome/css/header.css', 'link', 'head', 'stylesheet'
        );
    } else {
        angular.element(function () {
            angular.bootstrap(document, ['appModule']);
        });
    }
}(window, window.angular, window.angularAppModule));
